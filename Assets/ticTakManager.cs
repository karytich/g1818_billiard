﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ticTak
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private Text playerImage;
        [SerializeField]
        private Text iaImage;
        [SerializeField]
        private Text message;



        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                playerImage.text = "Камень";
                string ai = GetAi();
                iaImage.text = ai;
                CheckToWin("Камень", ai);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                playerImage.text = "Ножницы";
                string ai = GetAi();
                iaImage.text = ai;
                CheckToWin("Ножницы", ai);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                playerImage.text = "Бумага";
                string ai = GetAi();
                iaImage.text = ai;
                CheckToWin("Бумага", ai);
            }
        }

        private string GetAi()
        {
            int rnd = Random.Range(0, 3);
            switch (rnd)
            {
                case 0:
                    return "Бумага";


                case 1:
                    return "Ножницы";


                case 2:
                    return "Камень";


                default:
                    return string.Empty;

            }
        }

        private void CheckToWin(string player, string ai)
        {
            if (player == "Бумага")
            {
                if (ai == "Камень")
                    SetWin();
                if (ai == "Ножницы")
                    SetLost();
                if (ai == "Бумага")
                    SetDrow();
            }
            if (player == "Камень")
            {
                if (ai == "Камень")
                    SetDrow();
                if (ai == "Ножницы")
                    SetWin();
                if (ai == "Бумага")
                    SetLost();
            }
            if (player == "Ножницы")
            {
                if (ai == "Камень")
                    SetLost();
                if (ai == "Ножницы")
                    SetDrow();
                if (ai == "Бумага")
                    SetWin();
            }
        }

        private void SetWin()
        {
            message.text = "Победа";
        }

        private void SetLost()
        {
            message.text = "Проигрыш";
        }

        private void SetDrow()
        {
            message.text = "Ничья";

        }

    }
}
