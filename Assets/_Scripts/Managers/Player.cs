﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class Player : MonoBehaviour
{ 
    public string Name;
    public int Gold;
    public Sprite Avatar;
	
}
