﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab.ClientModels;
using PlayFab;
using UnityEngine.UI;
using System;


[Serializable]
public class PlayfabManager:MonoBehaviour
{
    private string NameFromFB;

    

    public void CreatePlayerOnLoginWithFacebook()
    {
        SetMessage("PlayFab Facebook try to login" );
        PlayFabClientAPI.LoginWithFacebook(new LoginWithFacebookRequest { CreateAccount = true, AccessToken = Facebook.Unity.AccessToken.CurrentAccessToken.TokenString },
                OnPlayfabFacebookAuthComplete, OnPlayfabFailed);
       
    }

    private void OnPlayfabFacebookAuthComplete(LoginResult result)
    {
        if(result.NewlyCreated)
        {
            Facebook.Unity.FB.API("me?fields=id,name,email,picture{url}", Facebook.Unity.HttpMethod.GET, UpdatePlayfabUserInfo);
            GetPlayerStatisticsRequest request = new GetPlayerStatisticsRequest();
            request.StatisticNames = new List<string>();
            request.StatisticNames.Add("leaderboard");
            PlayFabClientAPI.GetPlayerStatistics(request, GetUserStatsCallback, OnPlayfabFailed);
        }
        SetMessage("PlayFab Facebook Auth Complete. Session ticket: " + result.SessionTicket);
        
        PlayFabClientAPI.GetUserInventory(new GetUserInventoryRequest(), onGetInventoryCallback, OnPlayfabFailed);
    }    
    
    private void OnPlayfabFailed(PlayFabError error)
    {
        SetMessage("PlayFab Failed: " + error.GenerateErrorReport(), true);
    }

    public void SetMessage(string message, bool error = false)
    {
        string _message;
        _message = message;
        if (error)
            Debug.LogError(_message);
        else
            Debug.Log(_message);
    }

    private void UpdatePlayfabUserInfo(Facebook.Unity.IResult result)
    {
        SetMessage("GetUserInfoFromFacebook");
        if (result.Error == null)
        {
            NameFromFB = result.ResultDictionary["name"].ToString();
            var dictionary = (Dictionary<string, object>)result.ResultDictionary["picture"];
            var friendsList = (Dictionary<string, object>)dictionary["data"];
            string url = ((Dictionary<string, object>)friendsList)["url"].ToString();
            string Email = result.ResultDictionary["email"].ToString();

            
            PlayFabClientAPI.UpdateAvatarUrl(new UpdateAvatarUrlRequest() {ImageUrl = url }, OnPlayfabUpdateAvatarUrl, OnPlayfabFailed);
            PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest() { DisplayName = NameFromFB }, OnPlayfabUpdateName, OnPlayfabFailed);
            PlayFabClientAPI.AddOrUpdateContactEmail(new AddOrUpdateContactEmailRequest() { EmailAddress = Email }, OnUpdateEmail, OnPlayfabFailed);
        }
    }

    private void OnPlayfabUpdateAvatarUrl(EmptyResponse result)
    {
        SetMessage("Update avatar");
        
        GetAccountInfoRequest request = new GetAccountInfoRequest();
       // PlayFabClientAPI.GetAccountInfo(request, OnGetAccountInfo, OnPlayfabFailed);
    }

    private  void OnPlayfabUpdateName(UpdateUserTitleDisplayNameResult result)
    {
        SetMessage("Update name");
    }

    private void OnUpdateEmail(AddOrUpdateContactEmailResult result)
    {
        SetMessage("Update email");
    }

    public  void LogOut()
    {
        PlayFabClientAPI.ForgetAllCredentials();
    }

    public void AddGoldOnDB(int amount)
    {
        AddUserVirtualCurrencyRequest request = new AddUserVirtualCurrencyRequest();
        request.VirtualCurrency = StaticStrings.GoldCode;
        request.Amount = amount;
        PlayFabClientAPI.AddUserVirtualCurrency(request, OnUpdateGold, OnPlayfabFailed);
    }

    public void SubtractGoldOnDB(int amount)
    {
        SubtractUserVirtualCurrencyRequest request = new SubtractUserVirtualCurrencyRequest();
        request.VirtualCurrency = StaticStrings.GoldCode;
        request.Amount = amount;
        PlayFabClientAPI.SubtractUserVirtualCurrency(request, OnUpdateGold, OnPlayfabFailed);        
    }

    private void OnUpdateGold(ModifyUserVirtualCurrencyResult result)
    {
        if (result.VirtualCurrency == StaticStrings.GoldCode)
        {
            Managers.Instance.Player.Gold = result.Balance;            
        }
    }

    private void onGetInventoryCallback(GetUserInventoryResult result)
    {
        Managers.Instance.Player.Gold = result.VirtualCurrency[StaticStrings.GoldCode];
    }

    private void GetUserStatsCallback(GetPlayerStatisticsResult result)
    {
        Debug.Log(result.Request.ToString());
    }

    public void UpdateUserStatistic(int score)
    {
        StatisticUpdate statisticUpdate = new StatisticUpdate();
        statisticUpdate.StatisticName = "leaderboard";
        statisticUpdate.Value = score;
        UpdatePlayerStatisticsRequest request = new UpdatePlayerStatisticsRequest();
        request.Statistics = new List<StatisticUpdate>();
        request.Statistics.Add(statisticUpdate);
        PlayFabClientAPI.UpdatePlayerStatistics(request, UpdatePlayerStaticticCallback, OnPlayfabFailed);
    }

    private void UpdatePlayerStaticticCallback(UpdatePlayerStatisticsResult result)
    {
        Debug.Log(result.Request.ToString());
    }

    public void GetLeaderboard()
    {
        GetLeaderboardRequest request = new GetLeaderboardRequest();
        request.MaxResultsCount = 50;
        request.StartPosition = 0;
        request.StatisticName = "leaderboard";
        
        PlayFabClientAPI.GetLeaderboard(request, onLeaderboardCallback, OnPlayfabFailed);
    }

    private void onLeaderboardCallback(GetLeaderboardResult result)
    {
        Managers.Instance.Leaderboard = result.Leaderboard;
        Managers.Instance.ShowLeaderboard();
    }




}
