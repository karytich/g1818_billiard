﻿using UnityEngine;
using UnityEngine.Advertisements;
using PlayFab;
using System;

[Serializable]
public class AdsManager:MonoBehaviour
{
#if UNITY_IOS
    private string IOSGameId = StaticStrings.IosAdsID;
#elif UNITY_ANDROID   
    private string AndroidGameId = StaticStrings.AndroidAdsId;
#endif   
    public static bool IsTest = true;
   
    private void Awake()
    {
#if UNITY_IOS
        Advertisement.Initialize(IOSGameId, IsTest);
#elif UNITY_ANDROID       
        Advertisement.Initialize(AndroidGameId, IsTest);
#endif
    }

    public void ShowADS()
    {
        if (Advertisement.IsReady())
            Advertisement.Show();
    }

    public void ShowRewardedVideo()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResultCoins;
        Advertisement.Show("rewardedVideo", options);
    }   

    private void HandleShowResultCoins(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            Managers.Instance.AddGold(50);
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
    

}
