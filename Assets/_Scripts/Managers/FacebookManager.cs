﻿using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class FacebookManager:MonoBehaviour
{
    public string MyID;
   
    List<string> MyFriendNameList = new List<string>();
    List<string> MyFriendIDList = new List<string>();
    Dictionary<string, string> MyFriendDictionary = new Dictionary<string, string>();


    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void LoginWithFacebook()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    public void FacebookLogout()
    {
        FB.LogOut();
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            MyID = AccessToken.CurrentAccessToken.UserId;
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
            FB.API("/me/friends", HttpMethod.GET, GetFriendsPlayingThisGame);
            Managers.Instance.playfabManager.CreatePlayerOnLoginWithFacebook();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void DisplayUsername(IResult result)
    {
        if (result.Error == null)
        {
            Managers.Instance.Player.Name = result.ResultDictionary["first_name"].ToString();
        }
        else
        {
            Managers.Instance.Player.Name = StaticStrings.ErrorUserName;
            Debug.Log(result.Error);
        }
    }

    void DisplayProfilePic(IGraphResult result)
    {
        if (result.Texture != null)
        {
            Managers.Instance.Player.Avatar = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
        }
    }

    public void GetFriendsPlayingThisGame(IResult result)
    {
        if (result.Error == null)
        {
            var dictionary = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);

            var friendsList = (List<object>)dictionary["data"];
            foreach (var dict in friendsList)
            {
                MyFriendNameList.Add(((Dictionary<string, object>)dict)["name"].ToString());
                MyFriendIDList.Add(((Dictionary<string, object>)dict)["id"].ToString());
                MyFriendDictionary.Add(((Dictionary<string, object>)dict)["id"].ToString(), ((Dictionary<string, object>)dict)["name"].ToString());
            }
        }
    }


}
