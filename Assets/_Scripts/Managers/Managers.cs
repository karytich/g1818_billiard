﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour
{
    public static Managers Instance { get; private set; }

    public FacebookManager facebookManager;
    public PlayfabManager playfabManager;
    public Player Player;
    public AdsManager adsManager;

    public List<PlayFab.ClientModels.PlayerLeaderboardEntry> Leaderboard;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        adsManager = gameObject.AddComponent<AdsManager>();
        Player =  gameObject.AddComponent<Player>();
        playfabManager = gameObject.AddComponent<PlayfabManager>();
        facebookManager = gameObject.AddComponent<FacebookManager>();
       
    }

    public void LoginWithFacebook()
    {
        facebookManager.LoginWithFacebook();
    }

    public void SetAvatar(Sprite sprite)
    {
        Player.Avatar = sprite;
    }

    public void AddGold(int amount)
    {
        playfabManager.AddGoldOnDB(amount);
    }

    public void SubstractGold(int amount)
    {
        playfabManager.SubtractGoldOnDB(amount);
    }

    public void UpdateLeaderboard(int amount)
    {
        playfabManager.UpdateUserStatistic(amount);
    }

    public void GetLeaderboard()
    {
        playfabManager.GetLeaderboard();
    }

    public void ShowLeaderboard()
    {
        foreach (var item in Leaderboard)
        {
            Debug.Log(item.Position +": "+ item.DisplayName);
        }
    }

    public void ShowInternalAds()
    {
        adsManager.ShowADS();
    }

    public void ShowRewarAds()
    {
        adsManager.ShowRewardedVideo();
    }

}
