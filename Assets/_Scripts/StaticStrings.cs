﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticStrings
{
    public static string ErrorUserName = "User";
    public static string GoldCode = "GL";
    public static string IosAdsID = "2844704";
    public static string AndroidAdsId = "2844705";
}
