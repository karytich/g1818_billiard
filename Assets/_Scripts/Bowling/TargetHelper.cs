﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetHelper : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private bool moveToLeft;

    private Coroutine moveCor;

    private float m_x;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        moveCor = StartCoroutine(MoveTargetDirection());
    }


    public IEnumerator MoveTargetDirection()
    {
        while(true)
        {
            if(moveToLeft)
            {
                lineRenderer.SetPosition(0, Vector3.zero);
                m_x = Mathf.MoveTowards(m_x, 3, 0.1f);
                lineRenderer.SetPosition(1, new Vector3(m_x, 0.1f, -10));
               
                if (m_x==3)
                {
                    moveToLeft = false;
                }
            }
            else
            {
                lineRenderer.SetPosition(0, Vector3.zero);
                m_x = Mathf.MoveTowards(m_x, -3, 0.1f);
                lineRenderer.SetPosition(1, new Vector3(m_x, 0.1f, -10));
                if (m_x == -3)
                {
                    moveToLeft = true;
                }
            }
            yield return null;
        }
    }

    private void OnDestroy()
    {
        StopCoroutine(moveCor);
    }

    public Vector3 targetDirection()
    {
        return new Vector3(m_x, 3-Mathf.Abs(m_x), -10f);
    }

    public void Stop()
    {
        if(moveCor != null)
        {
            StopCoroutine(moveCor);
            moveCor = null;
        }
    }
	
}
