﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackBoard : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var ball = other.GetComponent<BallController>();
        if(ball != null)
        {
            FindObjectOfType<PinKeaper>().StartAwaiting();
            FindObjectOfType<BallController>().Stop();
        }

        var pin = other.GetComponent<Pin>();
        if(pin != null)
        {
            pin.StopMove();
        }
    }

}
