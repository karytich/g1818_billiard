﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour
{
    private Rigidbody m_rb;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
    }


    public bool CheckForStop()
    {
        if(m_rb.velocity.magnitude > 0.1f)
        {
            StopMove();
            return true;
        }
        return false;
    }

    public bool IsFallDown()
    {
        Vector3 rot = transform.rotation.eulerAngles;
        if(Mathf.Abs(rot.x) > 45 || Mathf.Abs(rot.y) > 45 || Mathf.Abs(rot.z) > 45)
        {
            return true;
        }
        return false;
    }

    public void StopMove()
    {
       // m_rb.isKinematic = true;
    }

    public void SetReadyToPlay()
    {
        m_rb.isKinematic = false;
    }

}
