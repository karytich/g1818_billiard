﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingManager : MonoBehaviour
{
    [SerializeField]
    private BallController m_ball;
    [SerializeField]
    private PinKeaper m_pinKeaper;
    [SerializeField]
    private TargetHelper targetHelper;

	public void Fail()
    {
        m_ball.Stop();
    }

    private void Update()
    {
       if(Input.GetKeyDown("space"))
        {
            TryToMakeShot();
        }
    }

    private void TryToMakeShot()
    {
        
            m_ball.SetStartPos();
            m_ball.Shot(targetHelper.targetDirection(), 1000f);
        
    }

}
