﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var ball = other.GetComponent<BallController>();
        if(ball != null)
        {
            FindObjectOfType<BowlingManager>().Fail();
        }

        var pin = other.GetComponent<Pin>();
        if (pin != null)
        {
            pin.StopMove();
        }
    }
}
