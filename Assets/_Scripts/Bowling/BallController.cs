﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private Rigidbody m_rb;
    private MeshRenderer renderer;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody>();
        renderer = GetComponent<MeshRenderer>();
    }

    public void Shot(Vector3 direction, float power)
    {
        m_rb.isKinematic = false;
        renderer.enabled = true;
        m_rb.AddForce(direction * power);
    }

    public void Stop()
    {
        m_rb.isKinematic = true;
        renderer.enabled = false;
    }

    public void SetStartPos()
    {
        m_rb.position = new Vector3(0, GetComponent<SphereCollider>().radius, 0);
    }
}
