﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinKeaper : MonoBehaviour
{
    private Pin[] pins;
    private Vector3[] startPos;
    private Quaternion[] startRot;

    private void Awake()
    {
        pins = GetComponentsInChildren<Pin>();
        startPos = new Vector3[pins.Length];
        startRot = new Quaternion[pins.Length];
        for (int i = 0; i < pins.Length; i++)
        {
            startPos[i] = pins[i].transform.position;
            startRot[i] = pins[i].transform.rotation;
        }
    }

    public void ResetPos()
    {
        for (int i = 0; i < pins.Length; i++)
        {
            pins[i].transform.position = startPos[i];
            pins[i].transform.rotation = startRot[i];
            pins[i].SetReadyToPlay();
        }
    }

    public void StartAwaiting()
    {
        //StartCoroutine(WaitForStopMove());
    }

    private IEnumerator WaitForStopMove()
    {
        int temp = 0;

        while (temp != pins.Length)
        {
            for (int i = 0; i < pins.Length; i++)
            {
                if (pins[i].CheckForStop())
                {
                    temp++;
                }
            }
            yield return null;
        }

        CheckForFall();
    }

    private int CheckForFall()
    {
        int temp= 0;
        for (int i = 0; i < pins.Length; i++)
        {
            if(pins[i].IsFallDown())
            {
                temp++;
            }
        }
        return temp;
    }

}
