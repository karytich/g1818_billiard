﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUIController : MonoBehaviour
{
    [SerializeField] private Image avatar;
	

    public void SetAvatar(Sprite sprite)
    {
        avatar.sprite = sprite;
    }
}
